import { NumbersCollection } from './NumbersCollection'
import { CharactersCollection } from './CharactersCollection'
import { LinkedList } from './LinkedList'

const numbersCollection = new NumbersCollection([3, 10, 5, -1, 12, -6])
numbersCollection.sort()
console.log('numbers collection sorted:', numbersCollection.data)
const charactersCollection = new CharactersCollection('stringifier')
charactersCollection.sort()
console.log('characters collection sorted:', charactersCollection.data)

const linkedList = new LinkedList()
linkedList.add(500)
linkedList.add(-10)
linkedList.add(-3)
linkedList.add(20)
linkedList.sort()
linkedList.print()
